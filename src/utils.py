#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
General utils
'''

import datetime
import re
import subprocess

import dateparser
import nltk
import numpy
from dateutil.parser import parse as dateutil_parse_date
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

from config import *
import string

stemmer = SnowballStemmer('english')
stop_words = stopwords.words('english')

class Aspect:
    def __init__(self, message):

        self.is_emi = False
        self.is_credit_card = False
        self.is_restaurant = False
        self.is_cab = False
        self.is_healthcare = False
        self.is_shopping = False
        self.is_salary = False

        self.category = None
        self.message = message
        self.account_number = None
        self.credit_card_number = None
        self.amount = None
        self.date = None
        self.employer_name = None


def fread(filename, mode='r'):
    return open(filename, mode).read().strip()

def write_file(log, filename, mode = 'a+'):
    f = open(filename, mode)
    f.write(log + '\n')
    f.close()

def preprocess_single_str(message, lowercase=True, tokenize=True, stem=True):
    '''
    Takes a single message as str.
    Returns a list of tokens after removing stopwords and stemming.
    '''
    if lowercase:
        message = message.lower()
    if tokenize:
        message = nltk.word_tokenize(message)
    else:
        message = message.split()
    _message = [i for i in message]
    message = []
    for i in _message:
        if i not in stop_words:
            if stem:
                message.append(stemmer.stem(i))
            else:
                message.append(i)
    
    message = '*&^%$#'.join(message)
    message = message.replace('rs*&^%$#.', 'rs.')
    message = message.replace('a/c*&^%$#.', 'a/c.')

    message = message.split('*&^%$#')

    return message
    

def preprocess(message, lowercase=True, tokenize=True, stem=True, lang='english'):
    if lang != 'english':
        raise Exception('Only English language is supported')
    
    if type(message) == list:
        res = []
        for cur_message in message:
            res.append(preprocess_single_str(cur_message, lowercase, tokenize, stem))
    elif type(message) == str:
        res = [preprocess_single_str(message, lowercase, tokenize, stem)]
    return res


def is_valid_date(date):
    if len(date) == 1:
        f = False
        for i in ALL_MONTHS:
            if i[0] in date:
                if date[0] == i[0]:
                    pass
                else:
                    f = True
                    break
            if i[1] in date:
                if date[0] == i[1]:
                    pass
                else:
                    f = True
                    break
        if ('/' in date) or ('-' in date) or ('|' in date) or ('.' in date): 
            f = True
        if f == False:
            return False
    if len(date) == 2:
        for i in ALL_MONTHS:
            if i[0] in date or i[1] in date:
                return False
    date = ' '.join(date)
    try:
        res = dateutil_parse_date(date, fuzzy=True)
    except ValueError:
        return False
    if res is not None:
        return True
    return False


def identify_date(message):
    message = message.split()
    L = len(message)
    for i in range(L):
        cur = []
        L1 = min(i + 3, L)
        for j in range(i, L1):
            cur.append(message[j])
            if cur == []:
                continue
            cur1 = ' '.join(cur[:])
            if not is_valid_date(cur):
                continue
            if cur1 == '.':
                continue
            try:
                res = dateparser.parse(cur1)
            except IndexError:
                continue
            if res is not None:
                return res
    return None

def identify_amount(message):
    message = message.split(' ')
    L = len(message)
    related_tokens = ['rs.', 'rs', 'rupees', 'inr']
    res = None
    for i in range(L):
        for j in related_tokens:
            if message[i] == j:
                if i - 1 >= 0:
                    try:
                        tmp = float(message[i - 1].replace(',', ''))
                        res = tmp
                    except ValueError:
                        pass
                if i + 1 < L:
                    try:
                        tmp = float(message[i + 1].replace(',', ''))
                        if res is not None:
                            #FIXME
                            pass
                        res = tmp
                    except ValueError:
                        pass
            else:
                if j in message[i]:
                    try:
                        tmp = float(message[i].replace(j, '').replace(',', ''))
                        if res is not None:
                            #FIXME
                            pass
                        res = tmp
                    except ValueError:
                        pass

    return res

def is_valid_account_number(acnumber):
    while len(acnumber) > 0:
        if acnumber[0] == 'x':
            acnumber = acnumber[1:]
        else:
            break
    while len(acnumber) > 0:
        if acnumber[-1] == 'x':
            acnumber = acnumber[:-1]
        else:
            break
    try:
        _ = int(acnumber)
        return True
    except ValueError:
        return False

def identify_account_number(message):
    message = message.split(' ')
    L = len(message)
    related_tokens = ['a/c.', 'account number', 'accountnumber', 'ac', 'a/c no.', 'ac no', 'a/c no', 'ac no.', 'no.', 'a/c']
    res = None
    for i in range(L):
        for j in related_tokens:
            if j == message[i]:
                if i + 1 < L:
                    for k in related_tokens:
                        tok = message[i + 1].replace(k, '')
                        if is_valid_account_number(tok):
                            return tok
            if 'xx' in message[i]:
                for k in related_tokens:
                        tok = message[i].replace(k, '')
                        if is_valid_account_number(tok):
                            return tok
    return res

def is_salary(message):
    for i in SALARY_IDENTIFIERS:
        if i in message:
            return True
    return False

def is_restaurant_category(message):
    for i in RESTAURANT_IDENTIFIERS:
        if i in message:
            return True
    return False

def is_shopping_category(message):
    for i in SHOPPING_IDENTIFIERS:
        if i in message:
            return True
    return False

def is_cab_category(message):
    for i in TRAVEL_IDENTIFIERS:
        if i in message:
            return True
    return False

def is_healthcare_category(message):
    for i in HEALTHCARE_IDENTIFIERS:
        if i in message:
            return True
    return False

def identify_name(message):
    name = None
    message = message.split()
    _message = []
    for i in message:
        if i not in string.punctuation:
            _message.append(i)
    message = message[:5]
    if len(message) <= 1:
        return name
    ignore = ['customer', 'sbi', 'a/c', 'a/c.'] + ['a/c.', 'account number', 'accountnumber', 'ac', 'a/c no.', 'ac no', 'a/c no', 'ac no.', 'no.', 'a/c']
    if message[0] == 'hello' or message[0] == 'dear':
        for i in ignore:
            if i in message[1] or i in '~!@#$%^&*()_+<>?:"\{\}][;\'/.,':
                return name
        name = message[1]
    return name

def classify_and_identify_aspects(message):
    res = Aspect(message)
    _message = ' '.join(message)
    res.date = identify_date(_message)
    res.amount = identify_amount(_message)
    res.account_number = identify_account_number(_message)
    res.employer_name = identify_name(_message)

    if is_salary(_message):
        res.is_salary = True
        res.category = 'salary'
    else:
        if 'credit card' in _message:
            res.is_credit_card = True
            res.credit_card_number = res.account_number
            res.account_number = None
        if 'emi' in _message or 'installment' in _message:
            res.is_emi = True
        
        # Right now I am using Rule based and heuristics to determine the category of a message. This might not be always correct.
        # The best way is to use Machine Learning classifier and topic modelling. But it requires huge data which I could not collect.
        if is_restaurant_category(_message):
            res.is_restaurant = True
            res.category = 'restaurants'
        if is_healthcare_category(_message):
            res.is_healthcare = True
            res.category = 'healthcare'
        if is_cab_category(_message):
            res.is_cab = True
            res.category = 'cabs'
        if is_shopping_category(_message):
            res.is_shopping = True
            res.category = 'shopping'
    return res

def get_expenditure(table, category):
    expenditure = 0
    for aspect in table:
        if category == aspect.category:
            expenditure += aspect.amount
    return expenditure

def get_credit_card_bill(table, last_k_months):
    bill = 0
    current_date = datetime.datetime.now()
    for aspect in table:
        if aspect.is_credit_card:
            if current_date.month - aspect.date.month <= last_k_months:
                bill += aspect.amount
    return bill

def get_emi_details(table):
    amount = 0
    count = 0
    current_date = datetime.datetime.now()
    for i in table:
        if i.is_emi:
            if i.date > current_date:
                count += 1
                amount += i.amount
    return count, amount

def get_salary(table):
    salary = 0
    for i in table:
        salary += i.amount
    return salary