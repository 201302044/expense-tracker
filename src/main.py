#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Main module
Usage: python main.py <text file which contains all messages>
       Example: python main.py messages.txt
'''

import os
import sys

import nltk
nltk.download('stopwords')

import tqdm
from beautifultable import BeautifulTable

import utils

CREDIT_CARD_TABLE = []
RESTAURANTS_TABLE = []
HEALTH_TABLE = []
SHOPPING_TABLE = []
CABS_TABLE = []
EMI_TABLE = []
UNKNOWN_TABLE = []
BANK_ACCOUNTS = set()
SALARY_TABLE = []

# EMPLOYER_NAME: All possible names of a single person.
# For example if the name is chaitanya sai, some messages might contain name as "chaitanya" while some others
# might have "sai". So this EMPLOYER_NAME stores all possible variations.
# If the employer name is same in all the messages, then this variable should contain single element which is what we desire.
EMPLOYER_NAME = set() 


file_name = sys.argv[1] #File
all_messages = utils.fread(file_name).split('\n')

all_messages = utils.preprocess(all_messages, lowercase=True, tokenize=True, stem=False, lang='english')
print('Processing messages')
for message in tqdm.tqdm(all_messages):
    aspects = utils.classify_and_identify_aspects(message)
    if aspects.is_emi:
        EMI_TABLE.append(aspects)
    if aspects.is_credit_card:
        CREDIT_CARD_TABLE.append(aspects)
    if aspects.is_shopping:
        SHOPPING_TABLE.append(aspects)
    if aspects.is_cab:
        CABS_TABLE.append(aspects)
    if aspects.is_healthcare:
        HEALTH_TABLE.append(aspects)
    if aspects.is_restaurant:
        RESTAURANTS_TABLE.append(aspects)
    if aspects.is_salary:
        SALARY_TABLE.append(aspects)
    
    if aspects.employer_name is not None:
        EMPLOYER_NAME.add(aspects.employer_name)
    if aspects.account_number is not None:
        BANK_ACCOUNTS.add(aspects.account_number)

print('Done processing messages')
ongoing_emis_count, ongoing_emis_amount = utils.get_emi_details(EMI_TABLE)
last_3_months_credit_card_bill = utils.get_credit_card_bill(CREDIT_CARD_TABLE, 3)
restaurant_expenditure = utils.get_expenditure(RESTAURANTS_TABLE, 'restaurants')
healthcare_expenditure = utils.get_expenditure(HEALTH_TABLE, 'healthcare')
shopping_expenditure = utils.get_expenditure(SHOPPING_TABLE, 'shopping')
cabs_expenditure = utils.get_expenditure(CABS_TABLE, 'cabs')
salary = utils.get_salary(SALARY_TABLE)

results = [
    ['Employer name', list(EMPLOYER_NAME)[0]],
    ['Salary', salary],
    ['Number of ongoing EMI\'s', ongoing_emis_count],
    ['Amount of onging EMI\'s (Rs.)', ongoing_emis_amount],
    ['Last 3 months credit card bill (Rs.)', last_3_months_credit_card_bill],
    ['Expenditure on Restaurants (Rs.)', restaurant_expenditure],
    ['Expenditure on Healthcare (Rs.)', healthcare_expenditure],
    ['Expenditure on Shopping (Rs.)', shopping_expenditure],
    ['Expenditure on Cabs (Rs.)', cabs_expenditure],
    ['Unique Number of Bank Accounts', len(BANK_ACCOUNTS)]
]
print('Results:')
table_results = BeautifulTable()
table_results.column_headers = ['Query', 'Result']
for i in results:
    table_results.append_row(i)
print(table_results)
