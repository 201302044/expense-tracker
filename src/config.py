#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Hard coded values and other configurations.
'''

ALL_MONTHS = [('jan', 'january'), ('feb', 'february'), ('mar', 'march'), ('apr', 'april'), ('may', 'may'), ('jun', 'june'), ('jul', 'july'), ('aug', 'august'), ('sep', 'september'), ('oct', 'october'), ('nov', 'november'), ('dec', 'december')]
SALARY_IDENTIFIERS = ['credited', 'salary']
TRAVEL_IDENTIFIERS = ['travel', 'cab', 'ola', 'uber']
SHOPPING_IDENTIFIERS = ['shop', 'grocery', 'groceries', 'clothes', 'accessories']
HEALTHCARE_IDENTIFIERS = ['hospital', 'health', 'dental', 'ambulance']
RESTAURANT_IDENTIFIERS = ['food', 'restaurant', 'cafeteria']
