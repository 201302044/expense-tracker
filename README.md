# Expense-Tracker

Given a bunch of SMS�s, the system will extract the details such as employer name, unique number of bank accounts, credit card usage, ongoing EMI�s, money spent on food, cabs, etc,. And thus giving the user concise information about his financial transactions.


Install dependencies using:
    
```
$ pip install -U -r requirements.txt
```


Example usage:

```
$ python src/main.py data/sample_data.txt
```


Sample output for data from sample_data.txt:

![](https://i.imgur.com/XKY5NkF.png)
